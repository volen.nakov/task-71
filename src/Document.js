import { useState } from "react";

function Document(props) {
    const [state, setState] = useState(true)
    const handleScroll = (e) => {
        const bottom = e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight
        if (bottom) {
            setState(false)
        }
    }
    return (
        <div>
            <div class="title">{props.title}</div>
            <div class="content" onScroll={(e) => handleScroll(e)} style={{ overflow: "auto", height: 400, fontSize:50 }}>{props.content}</div>
            <button disabled={state}>I Agree</button>
        </div>
    )
}
export default Document;