import "./App.css";
import Document from "./Document";
import { useEffect, useState } from "react";

function App() {
  const [terms, setTerms] = useState("");
  const fetchTerms = () => {
    fetch("https://jaspervdj.be/lorem-markdownum/markdown.txt")
      .then((response) => response.text())
      .then((text) => setTerms(text))
  }
  useEffect(() => fetchTerms(), [])
  return (
    <div>
      <Document title={"Terms and Conditions"} content={terms} />
    </div>
  );
}

export default App;
